<?php
namespace cat_crash\bingplaces_business;

Class Business {
	
	private $StoreId;
	private $BusinessName;
	private $ChainName;
	private $AddressLine1;
	private $AddressLine2;
	private $City;
	private $Country;
	private $StateOrProvince;
	private $PhoneNumber;
	private $Categories;
	private $Latitude;
	private $Longitude;
	private $BusinessEmail;
	private $MainWebSite;
	private $FacebookAddress;
	private $TwitterAddress;
	private $Photos;
	private $MenuURL;
	private $RestaurantPrice;
	private $HotelStarRating;
	private $Npi;
	private $Amenities;
	private $Open24Hours;
	private $OperatingHours;
	private $HolidayHours;
	private $HideAddress;
	private $ZipCode;
	private $IsClosed;


	private $allowedCountries=['AT'=>'Austria',
								'AU'=>'Australia',
								'BR'=>'Brazil',
								'CA'=>'Canada',
								'CH'=>'Switzerland',
								'CN'=>'China',
								'DE'=>'Germany',
								'ES'=>'Spain',
								'GB'=>'United Kingdom',
								'HK'=>'Hong Kong',
								'IN'=>'India',
								'IT'=>'Italy',
								'MX'=>'Mexico',
								'TW'=>'Taiwan',
								'US'=>'United States of America'
								];

	private $allowedStateOrProvince=[
								'AT'=>['Burgenland'=>1,
										'Carinthia'=>2,
										'Lower Austria'=>3,
										'Upper Austria'=>4,
										'Salzburg'=>5,
										'Styria'=>6,
										'Tyrol'=>7,
										'Vorarlberg'=>8,
										'Vienna'=>9
										],
								'AU'=>['Australian Capital Territory'=>'ACT',
										'New South Wales'=>'NSW',
										'Northern Territory'=>'NT',
										'Queensland'=>'QLD',
										'South Australia'=>'SA',
										'Tasmania'=>'TAS',
										'Victoria'=>'VIC',
										'Western Australia'=>'WA'
										],
								'BR'=>['Tocantins'=>'TO',
										'Sao Paulo'=>'SP',
										'Santa Catarina'=>'SC',
										'Rio Grande do Norte'=>'RN',
										'Rondonia'=>'RO',
										'Roraima'=>'RR',
										'Rio Grande do Sul'=>'RS',
										'Sergipe'=>'SE',
										'Rio de Janeiro'=>'RJ',
										'Parana'=>'PR',
										'Para'=>'PA',
										'Paraiba'=>'PB',
										'Pernambuco'=>'PE',
										'Piaui'=>'PI',
										'Minas Gerais'=>'MG',
										'Mato Grosso do Sul'=>'MS',
										'Mato Grosso'=>'MT',
										'Maranhao'=>'MA',
										'Acre'=>'AC',
										'Alagoas'=>'AL',
										'Amazonas'=>'AM',
										'Amapa'=>'AP',
										'Bahia'=>'BA',
										'Ceara'=>'CE',
										'Distrito Federal'=>'DF',
										'Espirito Santo'=>'ES',
										'Goias'=>'GO'
										],
								'CA'=>['British Columbia'=>'BC',
										'Alberta'=>'AB',
										'Manitoba'=>'MB',
										'Northwest Territories'=>'NT',
										'Nova Scotia'=>'NS',
										'New Brunswick'=>'NB',
										'Newfoundland and Labrador'=>'NL',
										'Prince Edward Island'=>'PE',
										'Nunavut'=>'NU',
										'Ontario'=>'ON',
										'Quebec'=>'QC',
										'Saskatchewan'=>'SK',
										'Yukon'=>'YT'
										],
								'CH'=>['Zug'=>'ZG',
										'Zurich'=>'ZH',
										'Thurgau'=>'TG',
										'Ticino'=>'TI',
										'Valais'=>'VS',
										'Uri'=>'UR',
										'Vaud'=>'VD',
										'Solothurn'=>'SO',
										'Schwyz'=>'SZ',
										'St Gallen'=>'SG',
										'Schaffhausen'=>'SH',
										'Obwalden'=>'OW',
										'Neuchatel'=>'NE',
										'Nidwalden'=>'NW',
										'Lucerne'=>'LU',
										'Jura'=>'JU',
										'Aargau'=>'AG',
										'Appenzell Innerrhoden'=>'AI',
										'Bern'=>'BE',
										'Appenzell Ausserrhoden'=>'AR',
										'Basel-Landschaft'=>'BL',
										'Basel-Stadt'=>'BS',
										'Graubunden'=>'GR',
										'Fribourg'=>'FR',
										'Geneva'=>'GE',
										'Glarus'=>'GL'
										],
								'CN'=>['Beijing Municipality'=>'CN-11',
										'Tianjin Municipality'=>'CN-12',
										'Hebei Province'=>'CN-13',
										'Shanxi Province'=>'CN-14',
										'Inner Mongolia Autonomous Region'=>'CN-15',
										'Liaoning Province'=>'CN-21',
										'Jilin Province'=>'CN-22',
										'Heilongjiang Province'=>'CN-23',
										'Shanghai Municipality'=>'CN-31',
										'Jiangsu Province'=>'CN-32',
										'Zhejiang Province'=>'CN-33',
										'Anhui Province'=>'CN-34',
										'Fujian Province'=>'CN-35',
										'Jiangxi Province'=>'CN-36',
										'Shandong Province'=>'CN-37',
										'Henan Province'=>'CN-41',
										'Hubei Province'=>'CN-42',
										'Hunan Province'=>'CN-43',
										'Guangdong Province'=>'CN-44',
										'Guangxi Zhuang Autonomous Region'=>'CN-45',
										'Hainan Province'=>'CN-46',
										'Chongqing Municipality'=>'CN-50',
										'Sichuan Province'=>'CN-51',
										'Guizhou Province'=>'CN-52',
										'Yunnan Province'=>'CN-53',
										'Tibet Autonomous Region'=>'CN-54',
										'Shaanxi Province'=>'CN-61',
										'Gansu Province'=>'CN-62',
										'Qinghai Province'=>'CN-63',
										'Ningxia Hui Autonomous Region'=>'CN-64',
										'Xinjiang Uyghur Autonomous Region'=>'CN-65',
										'Hong Kong Special Administrative Region'=>'CN-91',
										'Macau Special Administrative Region'=>'92'
										],
								'DE'=>['Baden-Wurttemberg'=>'BW',
										'Bavaria'=>'BY',
										'Berlin'=>'BE',
										'Brandenburg'=>'BB',
										'Bremen'=>'HB',
										'Hesse'=>'HE',
										'Hamburg'=>'HH',
										'North Rhine-Westphalia'=>'NW',
										'Lower Saxony'=>'NI',
										'Mecklenburg-Vorpommern'=>'MV',
										'Schleswig-Holstein'=>'SH',
										'Saarland'=>'SL',
										'Rhineland-Palatinate'=>'RP',
										'Saxony'=>'SN',
										'Saxony-Anhalt'=>'ST',
										'Thuringia'=>'TH'
										],
								'ES'=>['Santa Cruz de Tenerife'=>'TF',
										'Toledo'=>'TO',
										'Valencia'=>'V',
										'Valladolid'=>'VA',
										'Valencian Comunidad'=>'VC',
										'Alava'=>'VI',
										'Saragossa'=>'Z',
										'Zamora'=>'ZA',
										'Soria'=>'SO',
										'Tarragona'=>'T',
										'Teruel'=>'TE',
										'Guipuzcoa'=>'SS',
										'Cantabria'=>'S',
										'Salamanca'=>'SA',
										'Segovia'=>'SG',
										'Seville'=>'SE',
										'Palencia'=>'P',
										'Orense'=>'OR',
										'Asturias'=>'O',
										'Basque country'=>'PV',
										'Balearics'=>'PM',
										'Pontevedra'=>'PO',
										'Murcia'=>'MU',
										'Navarre'=>'NA',
										'Melilla'=>'ML',
										'Lleida'=>'L',
										'Huesca'=>'HU',
										'Jaen'=>'J',
										'Lugo'=>'LU',
										'Rioja'=>'LO',
										'Leon'=>'LE',
										'Malaga'=>'MA',
										'Madrid'=>'M',
										'Huelva'=>'H',
										'Granada'=>'GR',
										'Guadalajara'=>'GU',
										'Las Palmas'=>'GC',
										'Girona'=>'GE',
										'Galicia'=>'GA',
										'Estremadura'=>'EX',
										'Castellon'=>'CS',
										'Catalonia'=>'CT',
										'Caceres'=>'CC',
										'Cuenca'=>'CU',
										'Ceuta'=>'CE',
										'Castile and Leon'=>'CL',
										'Castilla-La Mancha'=>'CM',
										'Canary Islands'=>'CN',
										'Ciudad Real'=>'CR',
										'Biscay'=>'BI',
										'Aragon'=>'AR',
										'Badajoz'=>'BA',
										'Barcelona'=>'B',
										'Avila'=>'AV',
										'Almeria'=>'AL',
										'Andalucfa'=>'AN',
										'Albacete'=>'AB',
										'Alicante'=>'A',
										'Burgos'=>'BU',
										'Corunna'=>'C',
										'Cadiz'=>'CA',
										'Cordova'=>'CO'
								],
								'GB'=>['Cambridgeshire'=>'CAM',
										'Caerphilly'=>'CAY',
										'Bury'=>'BUR',
										'Bristol'=>'BST',
										'Bracknell Forest'=>'BRC',
										'Bradford'=>'BRD',
										'Bromley'=>'BRY',
										'Bolton'=>'BOL',
										'Blackpool'=>'BPL',
										'Ballymena'=>'BLA',
										'Ballymoney'=>'BLY',
										'Bournemouth'=>'BMH',
										'Banbridge'=>'BNB',
										'Barnet'=>'BNE',
										'Brighton and Hove'=>'BNH',
										'Barnsley'=>'BNS',
										'Aberdeenshire'=>'ABD',
										'Aberdeen City'=>'ABE',
										'Argyll and Bute'=>'AGB',
										'County of Anglesey'=>'AGY',
										'Angus'=>'ANS',
										'Antrim'=>'ANT',
										'Ards'=>'ARD',
										'Armagh'=>'ARM',
										'Birmingham'=>'BIR',
										'Buckinghamshire'=>'BKM',
										'Bedfordshire'=>'BDF',
										'Barking and Dagenham'=>'BDG',
										'Bridgend'=>'BGE',
										'Blaenau'=>'BGW',
										'Blackburn with Darwen'=>'BBD',
										'Brent'=>'BEN',
										'Bexley'=>'BEX',
										'Belfast'=>'BFS',
										'Cornwall'=>'CON',
										'Coventry'=>'COV',
										'Cumbria'=>'CMA',
										'Camden'=>'CMD',
										'Carmarthenshire'=>'CMN',
										'Calderdale'=>'CLD',
										'Clackmannanshire'=>'CLK',
										'Coleraine'=>'CLR',
										'Bath and North East Somerset'=>'BAS',
										'Central Bedfordshire'=>'CBF',
										'Ceredigion'=>'CGN',
										'Craigavon'=>'CGV',
										'Channel Islands'=>'CHA',
										'Cheshire East'=>'CHE',
										'Cheshire'=>'CHS',
										'Cheshire West and Chester'=>'CHW',
										'Carrickfergus'=>'CKF',
										'Cookstown'=>'CKT',
										'Conwy'=>'CWY',
										'Castlereagh'=>'CSR',
										'Darlington'=>'DAL',
										'Derbyshire'=>'DBY',
										'Cardiff'=>'CRF',
										'Croydon'=>'CRY',
										'Dungannon'=>'DGN',
										'Dumfries and Galloway'=>'DGY',
										'Denbighshire'=>'DEN',
										'Derby'=>'DER',
										'Devon'=>'DEV',
										'Doncaster'=>'DNC',
										'Dundee City'=>'DND',
										'Dorset'=>'DOR',
										'Down'=>'DOW',
										'Londonderry (Derry)'=>'DRY',
										'Dudley'=>'DUD',
										'Durham'=>'DUR',
										'Falkirk'=>'FAL',
										'Essex'=>'ESS',
										'East Sussex'=>'ESX',
										'Fermanagh'=>'FER',
										'Ealing'=>'EAL',
										'East Ayrshire'=>'EAY',
										'Edinburgh'=>'EDH',
										'East Dunbartonshire'=>'EDU',
										'East Lothian'=>'ELN',
										'Eilean Siar'=>'ELS',
										'Enfield'=>'ENF',
										'England'=>'ENG',
										'East Renfrewshire'=>'ERW',
										'East Riding of Yorkshire'=>'ERY',
										'Fife'=>'FIF',
										'Flintshire'=>'FLN',
										'Gateshead'=>'GAT',
										'GlasgowCity'=>'GLG',
										'Gloucestershire'=>'GLS',
										'Greenwich'=>'GRE',
										'Halton'=>'HAL',
										'Hampshire'=>'HAM',
										'Havering'=>'HAV',
										'Hackney'=>'HCK',
										'Guernsey'=>'GSY',
										'Herefordshire'=>'HEF',
										'Hillingdon'=>'HIL',
										'Highland'=>'HLD',
										'Hammersmith and Fulham'=>'HMF',
										'Hounslow'=>'HNS',
										'Gwynedd'=>'GWN',
										'Hartlepool'=>'HPL',
										'Hertfordshire'=>'HRT',
										'Harrow'=>'HRW',
										'Haringey'=>'HRY',
										'Manchester'=>'MAN',
										'Magherafelt'=>'MFT',
										'Middlesbrough'=>'MDB',
										'Medway'=>'MDW',
										'Leeds'=>'LDS',
										'Luton'=>'LUT',
										'Leicester'=>'LCE',
										'Lincolnshire'=>'LIN',
										'Liverpool'=>'LIV',
										'Limavady'=>'LMV',
										'London'=>'LND',
										'Leicestershire'=>'LEC',
										'Lewisham'=>'LEW',
										'Larne'=>'LRN',
										'Lisburn'=>'LSB',
										'Islington'=>'ISL',
										'Inverclyde'=>'IVC',
										'Isle of Man'=>'IOM',
										'Isles of Scilly'=>'IOS',
										'Isle of Wight'=>'IOW',
										'Lancashire'=>'LAN',
										'Lambeth'=>'LBH',
										'Kensington and Chelsea'=>'KEC',
										'Kent'=>'KEN',
										'Kingston upon Thames'=>'KTT',
										'Knowsley'=>'KWL',
										'Jersey'=>'JSY',
										'Kingston upon Hull'=>'KHL',
										'Kirklees'=>'KIR',
										'Midlothian'=>'MLN',
										'Monmouthshire'=>'MON',
										'North Ayrshire'=>'NAY',
										'Merthyr Tydfil'=>'MTY',
										'Moyle'=>'MYL',
										'Milton Keynes'=>'MIK',
										'Merton'=>'MRT',
										'Moray'=>'MRY',
										'Northern Ireland'=>'NIR',
										'North Lanarkshire'=>'NLK',
										'North Lincolnshire'=>'NLN',
										'Northumberland'=>'NBL',
										'North East Lincolnshire'=>'NEL',
										'Newcastle upon Tyne'=>'NET',
										'Norfolk'=>'NFK',
										'Nottingham'=>'NGM',
										'Newham'=>'NWM',
										'Newport'=>'NWP',
										'North Somerset'=>'NSM',
										'North Down'=>'NDN',
										'Newtownabbey'=>'NTA',
										'Northamptonshire'=>'NTH',
										'Neath Port Talbot'=>'NTL',
										'Nottinghamshire'=>'NTT',
										'North Tyneside'=>'NTY',
										'Poole'=>'POL',
										'Portsmouth'=>'POR',
										'Powys'=>'POW',
										'Perth and Kinross'=>'PKN',
										'Plymouth'=>'PLY',
										'Peterborough'=>'PTE',
										'Redcar and Cleveland'=>'RCC',
										'Rochdale'=>'RCH',
										'Rhondda, Cynon, Taff'=>'RCT',
										'Redbridge'=>'RDB',
										'Reading'=>'RDG',
										'Renfrewshire'=>'RFW',
										'North Yorkshire'=>'NYK',
										'Newry and Mourne'=>'NYM',
										'Oldham'=>'OLD',
										'Omagh'=>'OMH',
										'Orkney Islands'=>'ORK',
										'Pembrokeshire'=>'PEM',
										'Oxfordshire'=>'OXF',
										'Suffolk'=>'SFK',
										'Sefton'=>'SFT',
										'South Gloucestershire'=>'SGC',
										'Scottish Borders'=>'SCB',
										'Scotland'=>'SCT',
										'Salford'=>'SLF',
										'Slough'=>'SLG',
										'South Lanarkshire'=>'SLK',
										'Sheffield'=>'SHF',
										'St. Helens'=>'SHN',
										'Shropshire'=>'SHR',
										'Sandwell'=>'SAW',
										'South Ayrshire'=>'SAY',
										'Rutland'=>'RUT',
										'Richmond upon Thames'=>'RIC',
										'Rotherham'=>'ROT',
										'Swansea'=>'SWA',
										'Swindon'=>'SWD',
										'Southwark'=>'SWK',
										'Stockport'=>'SKP',
										'Southend-on-Sea'=>'SOS',
										'Tameside'=>'TAM',
										'Solihull'=>'SOL',
										'Somerset'=>'SOM',
										'Sunderland'=>'SND',
										'Strabane'=>'STB',
										'Stoke-on-Trent'=>'STE',
										'Stirling'=>'STG',
										'Southampton'=>'STH',
										'Sutton'=>'STN',
										'Staffordshire'=>'STS',
										'Stockton-on-Tees'=>'STT',
										'South Tyneside'=>'STY',
										'Shetland Islands'=>'ZET',
										'West Yorkshire'=>'WYK',
										'York'=>'YOR',
										'Warwickshire'=>'WAR',
										'West Berkshire'=>'WBK',
										'West Dunbartonshire'=>'WDU',
										'Waltham Forest'=>'WFT',
										'Wigan'=>'WGN',
										'Vale of Glamorgan'=>'VGL',
										'Wiltshire'=>'WIL',
										'Wakefield'=>'WKF',
										'Walsall'=>'WLL',
										'West Lothian'=>'WLN',
										'Wales'=>'WLS',
										'Wolverhampton'=>'WLV',
										'Wandsworth'=>'WND',
										'Windsor and Maidenhead'=>'WNM',
										'Wokingham'=>'WOK',
										'Worcestershire'=>'WOR',
										'Wirral'=>'WRL',
										'Warrington'=>'WRT',
										'Wrexham'=>'WRX',
										'Westminster'=>'WSM',
										'West Sussex'=>'WSX',
										'Trafford'=>'TRF',
										'Tower Hamlets'=>'TWH',
										'Telford and Wrekin'=>'TFW',
										'Thurrock'=>'THR',
										'Surrey'=>'SRY',
										'Torbay'=>'TOB',
										'Torfaen'=>'TOF'
								],
								'IN'=>['Tamil Nadu'=>'TN',
										'Telangana'=>'TG',
										'Tripura'=>'TR',
										'Uttar Pradesh'=>'UP',
										'Uttarakhand'=>'UT',
										'West Bengal'=>'WB',
										'Sikkim'=>'SK',
										'Punjab'=>'PB',
										'Odisha (Orissa)'=>'OR',
										'Rajasthan'=>'RJ',
										'Puducherry'=>'PY',
										'Nagaland'=>'NL',
										'Madhya Pradesh'=>'MP',
										'Mizoram'=>'MZ',
										'Manipur'=>'MN',
										'Meghalaya'=>'ML',
										'Maharashtra'=>'MH',
										'Kerala'=>'KL',
										'Karnataka'=>'KA',
										'Jharkhand'=>'JH',
										'Jammu & Kashmir'=>'JK',
										'Lakshadweep'=>'LD',
										'Haryana'=>'HR',
										'Himachal Pradesh'=>'HP',
										'Gujarat'=>'GJ',
										'Goa'=>'GA',
										'Delhi'=>'DL',
										'Dadra & Nagar Haveli'=>'DN',
										'Chhattisgarh'=>'CT',
										'Daman & Diu'=>'DD',
										'Chandigarh'=>'CH',
										'Assam'=>'AS',
										'Arunachal Pradesh'=>'AR',
										'Andaman & Nicobar Islands'=>'AN',
										'Andhra Pradesh'=>'AP',
										'Bihar'=>'BR'
								],
								'IT'=>['Brindisi'=>'BR',
										'Belluno'=>'BL',
										'Bologna'=>'BO',
										'Benevento'=>'BN',
										'Barletta-Andria-Trani'=>'BT',
										'Brescia'=>'BS',
										'Bolzano'=>'BZ',
										'Campobasso'=>'CB',
										'Cagliari'=>'CA',
										'Como'=>'CO',
										'Ascoli Piceno'=>'AP',
										'L\'Aquila'=>'AQ',
										'Ancona'=>'AN',
										'Alessandria'=>'AL',
										'Agrigento'=>'AG',
										'Arezzo'=>'AR',
										'Aosta'=>'AO',
										'Avellino'=>'AV',
										'Asti'=>'AT',
										'Bari'=>'BA',
										'Bergamo'=>'BG',
										'Biella'=>'BI',
										'Chieti'=>'CH',
										'Caserta'=>'CE',
										'Carbonia-Iglesias'=>'CI',
										'Caltanissetta'=>'CL',
										'Cremona'=>'CR',
										'Cuneo'=>'CN',
										'Catania'=>'CT',
										'Cosenza'=>'CS',
										'Frosinone'=>'FR',
										'Fermo'=>'FM',
										'Catanzaro'=>'CZ',
										'Genoa'=>'GE',
										'Enna'=>'EN',
										'Foggia'=>'FG',
										'Florence'=>'FI',
										'Forlì-Cesena'=>'FC',
										'Ferrara'=>'FE',
										'Grosseto'=>'GR',
										'Gorizia'=>'GO',
										'Lecce'=>'LE',
										'Latina'=>'LT',
										'Lucca'=>'LU',
										'Livorno'=>'LI',
										'Lodi'=>'LO',
										'Monza e Brianza'=>'MB',
										'Macerata'=>'MC',
										'Messina'=>'ME',
										'Imperia'=>'IM',
										'Isernia'=>'IS',
										'Crotone'=>'KR',
										'Lecco'=>'LC',
										'Milan'=>'MI',
										'Mantua'=>'MN',
										'Modena'=>'MO',
										'Naples'=>'NA',
										'Matera'=>'MT',
										'Massa-Carrara'=>'MS',
										'Novara'=>'NO',
										'Potenza'=>'PZ',
										'Pavia'=>'PV',
										'Pesaro e Urbino'=>'PU',
										'Parma'=>'PR',
										'Pordenone'=>'PN',
										'Prato'=>'PO',
										'Rome'=>'RM',
										'Ravenna'=>'RA',
										'Reggio di Calabria'=>'RC',
										'Ragusa'=>'RG',
										'Rieti'=>'RI',
										'Reggio nell\'Emilia'=>'RE',
										'Pistoia'=>'PT',
										'Oristano'=>'OR',
										'Olbia-Tempio'=>'OT',
										'Nuoro'=>'NU',
										'Ogliastra'=>'OG',
										'Piacenza'=>'PC',
										'Padua'=>'PD',
										'Palermo'=>'PA',
										'Pescara'=>'PE',
										'Perugia'=>'PG',
										'Pisa'=>'PI',
										'Sondrio'=>'SO',
										'Savona'=>'SV',
										'Teramo'=>'TE',
										'Taranto'=>'TA',
										'La Spezia'=>'SP',
										'Syracuse'=>'SR',
										'Sassari'=>'SS',
										'Rovigo'=>'RO',
										'Rimini'=>'RN',
										'Salerno'=>'SA',
										'Siena'=>'SI',
										'Vibo Valentia'=>'VV',
										'Vercelli'=>'VC',
										'Verbano-Cusio-Ossola'=>'VB',
										'Venice'=>'VE',
										'Vicenza'=>'VI',
										'Verona'=>'VR',
										'Medio Campidano'=>'VS',
										'Viterbo'=>'VT',
										'Udine'=>'UD',
										'Treviso'=>'TV',
										'Varese'=>'VA',
										'Terni'=>'TR',
										'Turin'=>'TO',
										'Trieste'=>'TS',
										'Trento'=>'TN',
										'Trapani'=>'TP'
								],
								'MX'=>['Tlaxcala'=>'TLA',
										'Veracruz'=>'VER',
										'Zacatecas'=>'ZAC',
										'Yucatan'=>'YUC',
										'Sinaloa'=>'SIN',
										'San Luis Potosi'=>'SLP',
										'Quintana Roo'=>'ROO',
										'Tabasco'=>'TAB',
										'Tamaulipas'=>'TAM',
										'Sonora'=>'SON',
										'Oaxaca'=>'OAX',
										'Queretaro'=>'QUE',
										'Puebla'=>'PUE',
										'Nuevo Leon'=>'NLE',
										'Nayarit'=>'NAY',
										'Morelos'=>'MOR',
										'Michoacan'=>'MIC',
										'Jalisco'=>'JAL',
										'Mexico'=>'MEX',
										'Guerrero'=>'GRO',
										'Guanajuato'=>'GUA',
										'Hidalgo'=>'HID',
										'Distrito Federal'=>'DIF',
										'Durango'=>'DUR',
										'Coahuila'=>'COA',
										'Colima'=>'COL',
										'Chihuahua'=>'CHH',
										'Chiapas'=>'CHP',
										'Baja California'=>'BCN',
										'Baja California Sur'=>'BCS',
										'Aguascalientes'=>'AGU',
										'Campeche'=>'CAM'
								],
								'TW'=>['Changhua'=>'CHA',
										'Chiayi'=>'CYI',
										'Hsinchu'=>'HSZ',
										'Hualien'=>'HUA',
										'Kaohsiung'=>'KHH',
										'Ilan'=>'ILA',
										'Miaoli'=>'MIA',
										'Nantou'=>'NAN',
										'Pingtung'=>'PIF',
										'Penghu'=>'PEN',
										'Taoyuan'=>'TAO',
										'Yunlin'=>'YUN',
										'Taipei'=>'TPE',
										'Tainan'=>'TNN',
										'Taitung'=>'TTT',
										'Taichung'=>'TXG'
								],
								'US'=>['Virginia'=>'VA',
										'Texas'=>'TX',
										'Tennessee'=>'TN',
										'Utah'=>'UT',
										'Vermont'=>'VT',
										'Virgin Islands'=>'VI',
										'West Virginia'=>'WV',
										'Wyoming'=>'WY',
										'Washington'=>'WA',
										'Wisconsin'=>'WI',
										'South Carolina'=>'SC',
										'South Dakota'=>'SD',
										'Pennsylvania'=>'PA',
										'Ohio'=>'OH',
										'Oklahoma'=>'OK',
										'Nevada'=>'NV',
										'Oregon'=>'OR',
										'Puerto Rico'=>'PR',
										'Rhode Island'=>'RI',
										'Mississippi'=>'MS',
										'Montana'=>'MT',
										'Michigan'=>'MI',
										'Missouri'=>'MO',
										'Minnesota'=>'MN',
										'New Mexico'=>'NM',
										'New Jersey'=>'NJ',
										'Nebraska'=>'NE',
										'New Hampshire'=>'NH',
										'North Carolina'=>'NC',
										'North Dakota'=>'ND',
										'New York'=>'NY',
										'Kansas'=>'KS',
										'Kentucky'=>'KY',
										'Louisiana'=>'LA',
										'Iowa'=>'IA',
										'Idaho'=>'ID',
										'Illinois'=>'IL',
										'Indiana'=>'IN',
										'Maine'=>'ME',
										'Maryland'=>'MD',
										'Massachusetts'=>'MA',
										'Hawaii'=>'HI',
										'Guam'=>'GU',
										'Micronesia'=>'FM',
										'Florida'=>'FL',
										'Georgia'=>'GA',
										'Delaware'=>'DE',
										'District Of Columbia'=>'DC',
										'Connecticut'=>'CT',
										'California'=>'CA',
										'Colorado'=>'CO',
										'Alabama'=>'AL',
										'Alaska'=>'AK',
										'Arizona'=>'AZ',
										'Arkansas'=>'AR',
										'American Samoa'=>'AS',
										'PA'=>'PA'
								]
	];


	private $required=[ 'StoreId',
						'BusinessName',
						'AddressLine1',
						'City',
						'StateOrProvince',
						'Country',
						'ZipCode',
						'PhoneNumber',
						'Categories' ];


	public function __construct(array $_settings=null){

	}

	public function setStoreId($value){
		$this->StoreId=$value;
	}

	public function setBusinessName($value){
		$this->BusinessName=$value;
	}

	public function setChainName(\bingplaces\Chain $value){
		$this->ChainName=$value;
	}

	public function setAddressLine1($value){
		$this->AddressLine1=$value;
	}

	public function setAddressLine2($value){
		$this->AddressLine2=$value;
	}

	public function setCity($value){
		$this->City=$value;
	}
	
	public function setCountry($value){
		$offset=array_search($value, $this->allowedCountries);

		if(false === $offset){
			throw new \Exception("Country ".$value." not found in alowed countries list");
		}

		$this->Country=$offset;
	}
	
	public function setStateOrProvince($value){
		if(empty($this->Country)){
			throw new \Exception("Country needs to be defined before State/Province");
		}

		if(array_key_exists($this->Country, $this->allowedStateOrProvince)){
			$stateOrProvince=$this->allowedStateOrProvince[$this->Country];
			
			if(!array_key_exists($value, $stateOrProvince)){
				throw new \Exception("State or province ".$value." not found in alowed countries list");
			}

			$this->StateOrProvince=$stateOrProvince[$value];
		}

		
	}
	
	public function setPhoneNumber($value){
		$this->PhoneNumber=$value;
	}

	public function setCategories(Categories $value){
		$this->Categories=$value->getCategories();
	}

	public function setLatitude($value){
		$this->Latitude=filter_var($value, FILTER_VALIDATE_FLOAT);
	}

	public function setLongitude($value){
		$this->Longitude=filter_var($value, FILTER_VALIDATE_FLOAT);
	}

	public function setBusinessEmail($value){
		$this->BusinessEmail=filter_var($value, FILTER_VALIDATE_EMAIL);
	}

	public function setMainWebSite($value){
		$this->MainWebSite=filter_var($value, FILTER_VALIDATE_URL);
	}

	public function setFacebookAddress($value){
		$this->FacebookAddress=filter_var($value, FILTER_VALIDATE_URL);
	}

	public function setTwitterAddress($value){
		$this->TwitterAddress=filter_var($value, FILTER_VALIDATE_URL);
	}

	public function setPhotos(array $value){
		$this->Photos=$value;
	}

	public function setMenuURL($value){
		$this->MenuURL=filter_var($value, FILTER_VALIDATE_URL);
	}

	public function setRestaurantPrice($value){
		preg_match('/[\$]{1,5}/', $value, $matches);
		if(!empty($matches[0])){
			$this->RestaurantPrice=$matches[0];
		}
	}

	public function setHotelStarRating($value){
		preg_match('/[1-5]/', $value, $matches);
		if(!empty($matches[0])){
			$this->HotelStarRating=$matches[0];
		}
	}

	public function setAmenities(array $value){
		$this->Amenities=$value;
	}

	public function setNpi(integer $value){
		preg_match('/[\d]{10}/', filter_var($value, FILTER_VALIDATE_INT), $matches);
		if(!empty($matches[0])){
			$this->Npi=$matches[0];
		}
	}

	public function setOpen24Hours(bool $value){
		$this->Open24Hours=filter_var($value, FILTER_VALIDATE_BOOLEAN);
	}

	public function setOperatingHours(OperatingHours $value){
		$this->OperatingHours=$value->getOperationHours();
	}

	public function setHolidayHours($value){
		$this->HolidayHours=$value;
	}

	public function setHideAddress($value){
		$this->HideAddress=filter_var($value, FILTER_VALIDATE_BOOLEAN);
	}

	public function setIsClosed($value){
		$this->IsClosed=filter_var($value, FILTER_VALIDATE_BOOLEAN);
	}

	public function setZipCode($value){
		$this->ZipCode=$value;
	}

	public function getBusiness(){
		$return=[
				'StoreId'=>isset($this->StoreId)?$this->StoreId:'',
				'BusinessName'=>isset($this->BusinessName)?$this->BusinessName:'',
				'ChainName'=>isset($this->ChainName)?$this->ChainName:'',
				'AddressLine1'=>isset($this->AddressLine1)?$this->AddressLine1:'',
				'AddressLine2'=>isset($this->AddressLine2)?$this->AddressLine2:'',
				'City'=>isset($this->City)?$this->City:'',
				'Country'=>isset($this->Country)?$this->Country:'',
				'StateOrProvince'=>isset($this->StateOrProvince)?$this->StateOrProvince:'',
				'PhoneNumber'=>isset($this->PhoneNumber)?$this->PhoneNumber:'',
				'Categories'=>isset($this->Categories)?$this->Categories:'',
				'Latitude'=>isset($this->Latitude)?$this->Latitude:'',
				'Longitude'=>isset($this->Longitude)?$this->Longitude:'',
				'BusinessEmail'=>isset($this->BusinessEmail)?$this->BusinessEmail:'',
				'MainWebSite'=>isset($this->MainWebSite)?$this->MainWebSite:'',
				'FacebookAddress'=>isset($this->FacebookAddress)?$this->FacebookAddress:'',
				'TwitterAddress'=>isset($this->TwitterAddress)?$this->TwitterAddress:'',
				'Photos'=>isset($this->Photos)?$this->Photos:'',
				'MenuURL'=>isset($this->MenuURL)?$this->MenuURL:'',
				'RestaurantPrice'=>isset($this->RestaurantPrice)?$this->RestaurantPrice:'',
				'HotelStarRating'=>isset($this->HotelStarRating)?$this->HotelStarRating:'',
				'Npi'=>isset($this->Npi)?$this->Npi:'',
				'Amenities'=>isset($this->Amenities)?$this->Amenities:'',
				'Open24Hours'=>isset($this->Open24Hours)?$this->Open24Hours:'',
				'OperatingHours'=>isset($this->OperatingHours)?$this->OperatingHours:'',
				'HolidayHours'=>isset($this->HolidayHours)?$this->HolidayHours:'',
				'HideAddress'=>isset($this->HideAddress)?$this->HideAddress:'',
				'ZipCode'=>isset($this->ZipCode)?$this->ZipCode:'',
				'IsClosed'=>isset($this->IsClosed)?$this->IsClosed:''
		];

		return $return;

	}

	public function __destruct(){
		foreach($this->required as $required){
			if(empty($this->$required)){
				throw new \Exception("Property ".$required." is required");		
			}
		}
	}


}