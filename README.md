# PHP Wrapper for Bing Places API (UNOFICIAL)


## Installation

***NOTE:** Although this library may work with PHP 5.3^, it is only tested against PHP 7.2^ and we highly recommended to use PHP 7.2^. Additionally, 

### Install Client Library

**Install the Library via [Composer](http://getcomposer.org/):**

```shell
composer require cat_crash/bingplaces-business
```

**OR: Manually Install the Library**

Manually download this library and require it from your project:

```php
require_once("/path/to/lib/Bingplaces.php");
```

### Install Dependencies

Run the following from the root directory of the library:

```shell
composer install
```

##Usage##
```php
require('Bingplaces.php');
require('Business.php');
require('Categories.php');
require('Chain.php');
require('Identity.php');
require('OperatingHours.php');

$oh=new \bingplaces\OperatingHours(
	
	['Mon'=>['08:00 AM-12:00 AM','1:00 PM-06:00 PM'],
	 'Tue'=>'09:00 AM-06:00 PM',
	 'Wed'=>'09:00 AM-06:00 PM',
	 'Thu'=>'09:00 AM-06:00 PM',
	 'Fri'=>'09:00 AM-06:00 PM',
	 'Sat'=>'closed']
);

$business=new \bingplaces\Business();
$business->setStoreId('store1234');
$business->setBusinessName("Hello, world");
$business->setAddressLine1("5205 Harpers Xing");
$business->setCity("Langhorne");
$business->setCountry("United States of America");
$business->setStateOrProvince("Pennsylvania");
$business->setZipCode("19047");
$business->setPhoneNumber("+12158824588");
//$business->setRestaurantPrice("$$$");
$business->setOperatingHours($oh);
$business->setCategories(new \bingplaces\Categories(['Air Conditioning Services','Auto Tag Agency']));


$chain=new \bingplaces\Chain(['ChainName'=>'Hello-World',
							 'Website'=>'http://hello-world.com',
							 'Locations'=>100,
							 'ClientContactName'=>'Ivan Ivanov',
							 'ClientCorporateEmail'=>'none@none2.com'
							 ]);



$api=new \bingplaces\Bingplaces();

$api->setIdentity(new \bingplaces\Identity([
	'EmailId'=>'khvalov.valery@gmail.com',
	'Puid'=>'',
	'AuthProvider'=>1
	]));

$api->setCertificate('TP_API_Test.pfx','password!123');
$api->setTrackingId('af765c85-6a08-4e30-a064-30574599e1a4');
//$results=$api->CreateBulkChain($chain);
//$results=$api->UpdateBulkChainInfo($chain);

//$result=$api->createBusinesses($business);
//$result=$api->updateBusinesses($business);
//$results=$api->GetBusinessesByStoreId('store123');
//$results=$api->GetBusinessesBatches();
//$results=$api->GetBusinessesByAttributes(['City'=>'Langhorne']);
//$results=$api->GetAnalyticsByStoreId('store123');
//$results=$api->GetAnalyticsBatches();
//$results=$api->DeleteBusinesses('store123');
/*
$results=$api->GetBusinesses([
	"PageNumber"=>1,
 	"PageSize"=>100,
 	"SearchCriteria"=>[
		 "CriteriaType"=>"SearchByQuery",
		 "City"=>"Langhorne",
	 ],	
 	"TrackingId"=> $result['TrackingId']
]);
*/
/*

$status=$api->GetBusinessStatusInfo([
	"PageNumber"=>1,
 	"PageSize"=>100,
 	"CriteriaType"=>"GetInBatches",
 	"TrackingId"=> $result['TrackingId']
]);
*/

```