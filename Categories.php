<?php

namespace cat_crash\bingplaces_business;

class Categories {
	private $categories=[];

	public function __construct(array $array){

		$categoriesList=json_decode(file_get_contents(__DIR__.'/categories.json'),true);

		foreach ($array as $key => $value) {

			if (preg_match('/^[0-9]+$/', $value)) {
			  // Search by ID's
				if(array_key_exists($value, $categoriesList)){
					$this->categories[$value]=$categoriesList[$value];
				} else {
					throw new \Exception("Category ID:".$value." is not in official categories list");
				}

			} else {
			  // Search by name
				if(!in_array($value, $categoriesList)){
					throw new \Exception("Category name: ".$value." is not in official categories list");
					
				} else {
					$offset=array_search($value, $categoriesList); ;
					$this->categories[$offset]=$categoriesList[$offset];
				}
			}
		}
	}

	public function getCategories(){
		
		while(list($key,$value)=each($this->categories)){
			if(!empty($key) && !empty($value)){
				$return[]=[
					"CategoryName"=>$value,
		 			"BPCategoryId"=> $key
	 			];
 			}
		}

		return ["BusinessCategories"=>$return];
	}



}