<?php
namespace cat_crash\bingplaces_business;


Class Identity {
	private $settings=[
					'Puid'=>null,
					'AuthProvider'=>null,
					'EmailId'=>null];

	public function __construct($settings=null){
		if(is_array($settings) && array_key_exists('Puid', $settings) && array_key_exists('AuthProvider', $settings) && array_key_exists('EmailId', $settings)){
			$this->settings=$settings;
		}

	}

	public function setPuid($value){
		$this->settings['Puid']=$value;
	}

	public function setAuthProvider($value){
		$this->settings['AuthProvider']=$value;
	}

	public function setEmailId($value){
		$this->settings['EmailId']=$value;
	}

	public function getIdentity(){
		return $this->settings;
	}

}

?>