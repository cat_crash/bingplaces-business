<?php
/*
$oh=new \bingplaces\OperatingHours();
$oh->setMonday(['09:00','6:00 PM']);
$oh->setTuesday(['09:00','6:00 PM']);
$oh->setWednesday(['09:00','6:00 PM']);
$oh->setThursday(['09:00','6:00 PM']);
$oh->setFriday(['09:00','6:00 PM']);
$oh->setSaturday('closed');
$oh->setSunday('closed');
var_dump ($oh->getOperationHours());
or

$oh=new \bingplaces\OperatingHours(
	['Mon'=>'08:00 AM-06:00 PM',
	 'Tue'=>'08:00 AM-06:00 PM',
	 'Wed'=>'08:00 AM-06:00 PM',
	 'Thu'=>'08:00 AM-06:00 PM',
	 'Fri'=>'08:00 AM-06:00 PM',
	 'Sat'=>'closed',
	 'Sun'=>'closed']
);
var_dump ($oh->getOperationHours());

*/
namespace cat_crash\bingplaces_business; 
date_default_timezone_set('UTC');

Class OperatingHours{
	private $days=[];

	public function __construct($settings=null){
		if(is_array($settings)){	
			while(list($key,$value)=each($settings)){
				if(!empty($key)){
					call_user_func([$this,'set'.$key], $value);
				}
			}
		}
	}

	public function setSun($value){
		$this->setSunday($value);
	}

	public function setSunday($value){
		$this->days['Sun']=self::assignOperationHours($value);
	}

	public function setSat($value){
		$this->setSaturday($value);
	}

	public function setSaturday($value){
		$this->days['Sat']=self::assignOperationHours($value);
	}

	public function setFri($value){
		$this->setFriday($value);
	}

	public function setFriday($value){
		$this->days['Fri']=self::assignOperationHours($value);
	}

	
	public function setThu($value){
		$this->setThursday($value);
	}

	public function setThursday($value){
		$this->days['Thu']=self::assignOperationHours($value);
	}

	public function setWed($value){
		$this->setWednesday($value);
	}

	public function setWednesday($value){
		$this->days['Wed']=self::assignOperationHours($value);
	}

	public function setTue($value){
		$this->setTuesday($value);
	}

	public function setTuesday($value){
		$this->days['Tue']=self::assignOperationHours($value);
	}


	public function setMon($value){
		$this->setMonday($value);
	}

	public function setMonday($value){
		$this->days['Mon']=self::assignOperationHours($value);
	}


	public static function assignOperationHours($raw){
		// If data provided in array like: ['08:00 AM-12:00 AM','1:00 PM-06:00 PM']
		if(is_array($raw)){
			foreach($raw as $arrayItem){
				$matches[]=self::parseTimeString($arrayItem);
			}

		} else {
			// If data provided non in array but....
			switch($raw){
				case "closed":
					$matches=-1;
				break;

				case "24h":
					$matches[]=['0:00 AM','11:59 PM'];
				break;

				default:
					$matches[]=self::parseTimeString($raw);
				break;
			}
		}
		return $matches;
	}

	public static function parseTimeString($string){
		preg_match_all('/([\d]{1,2}:[\d]{1,2}[\s]?(am|pm)?)/i', $string, $matches, PREG_SET_ORDER);
		foreach($matches as $match){
			$return[]=self::formatTime(self::parseTime($match[0]));
		}
		return $return;
	}

	private static function parseTime($datestring){
		$_arr=date_parse( $datestring );

		if(!isset($_arr['hour']) || !isset($_arr['minute'])){
			throw new \Exception("Unable to parse datestring ".$datestring);	
		}

		return ['hour'=>$_arr['hour'],'minute'=>$_arr['minute']];
	}

	private static function formatTime($timeArray){
		if(isset($timeArray['hour']) && isset($timeArray['minute'])){
			$date = new \DateTime($timeArray['hour'].':'.$timeArray['minute']);
    		return $date->format('h:i A') ;
    	}

	}

	public function getOperationHours(){
		$return=[];
		while(list($key,$value)=each($this->days)){
			if($value != -1){
				foreach($value as $dayrecord){
					$return[]=$key." ".implode("-", $dayrecord);
				}
			}
		}
		return $return;
	}

}