<?php
namespace cat_crash\bingplaces_business;

//require('/Users/valery/www/mapkin/www/vendor/autoload.php');

use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\MessageFormatter;
use Monolog\Logger;
  
Class Bingplaces {
	
	public $baseApiUri='https://bptestwebsite.cloudapp.net/trustedPartnerApi/v1/';
	public $certificate=null;
	public $password=null;

	public $verbose=false;

	private $client;
	private $identity;
	private $business;

	private $trackingId;


	public function __construct(){

		$stack = HandlerStack::create();
		$stack->push(
		    Middleware::log(
		        new Logger('Logger'),
		        new MessageFormatter('{uri} - {code} - {req_body} - {res_body}')
		    )
		);


		$this->client = new Client([
		    // Base URI is used with relative requests
		    'base_uri' => $this->baseApiUri,
		    // You can set any number of default request options.
		    'timeout'  => 10.0,
		    'handler' => ($this->verbose)?$stack:null,
		]);

	}

	public function setCertificate($file,$password){
		$this->certificate=$file;
		$this->password=$password;
	}
	
	public function setIdentity(Identity $identity ){
		$this->identity=$identity;
	}

	public function setBusiness(Business $business ){
		$this->business=$business;
	}

	public function CreateBusinesses(Business $business){
		$this->checkCertAndPass();

		$business=$business->getBusiness();
		$identity=$this->identity->getIdentity();

		$resource=[
			"Businesses"=>[$business],
			"TrackingId"=>$this->trackingId,
			"Identity"=>$identity
		];
	
		$response=$this->client->request('POST', 'CreateBusinesses',['cert'=>[$this->certificate,$this->password],'json' => $resource]);
		
		if( $response->getStatusCode()!==200){
			throw new \Exception("HTTP return Error: ".$response->getStatusCode());
			return null;
		}

		$object=json_decode($response->getBody()->getContents(),true);
		
		if(is_array($object) && array_key_exists('TrackingId', $object)){
			$this->setTrackingId($object['TrackingId']);
		}

		return $object;
		
	}

	public function setTrackingId($id){
		$this->trackingId=$id;
	}


	public function UpdateBusinesses(Business $business){
		$this->checkCertAndPass();

		$business=$business->getBusiness();
		$identity=$this->identity->getIdentity();
		
		$resource=[
			"Businesses"=>[$business],
			"TrackingId"=>$this->trackingId,
			"Identity"=>$identity
		];
	
		$response=$this->client->request('POST', 'UpdateBusinesses',['cert'=>[$this->certificate,$this->password],'json' => $resource]);
		
		if( $response->getStatusCode()!==200){
			throw new \Exception("HTTP return Error: ".$response->getStatusCode(),true);
			return null;
		}

		return json_decode($response->getBody()->getContents(),true);

	}



	public function GetBusinessesByAttributes($_attributes){
		if(array_key_exists('BusinessName', $_attributes) || array_key_exists('City', $_attributes) || array_key_exists('BPCategoryId', $_attributes) || array_key_exists('Zip', $_attributes)){
			$_array=["PageNumber"=>1,
			 	"PageSize"=>100,
			 	"SearchCriteria"=>[
					 "CriteriaType"=>"SearchByQuery",
				 ],	
			 	"TrackingId"=> $this->trackingId
				];

				$_array["SearchCriteria"]=$_array["SearchCriteria"]+$_attributes;

			return $this->GetBusinessesBase($_array);

		} else {
			throw new \Exception("BusinessName and (or) City and (or) BPCategoryId and (or) Zip have to be defined search array", 1);
		}
		
	}



	public function GetBusinessesByStoreId($storeId){
		if(is_string($storeId)){
			$StoreIds[]=$storeId;
		} else {
			$StoreIds=$storeId;
		}

		$_array=["PageNumber"=>1,
			 	"PageSize"=>100,
			 	"SearchCriteria"=>[
					 "CriteriaType"=>"SearchByStoreIds",
					 "StoreIds"=> $StoreIds,
				 ],	
			 	"TrackingId"=> $this->trackingId
				];

		return $this->GetBusinessesBase($_array);
	}

	public function GetBusinessesBatches(){
		$_array=["PageNumber"=>1,
			 	"PageSize"=>100,
			 	"SearchCriteria"=>[
					 "CriteriaType"=>"GetInBatches",
				 ],	
			 	"TrackingId"=> $this->trackingId
				];
		return $this->GetBusinessesBase($_array);
	}

	public function GetBusinessesBase($_array){
		$this->checkCertAndPass();
		$identity=$this->identity->getIdentity();
		$resource=$_array+["Identity"=>$identity];

		$response=$this->client->request('POST', 'GetBusinesses',['cert'=>[$this->certificate,$this->password],'json' => $resource]);
		
		if( $response->getStatusCode()!==200){
			throw new \Exception("HTTP return Error: ".$response->getStatusCode(),true);
			return null;
		}

		return json_decode($response->getBody()->getContents());

	}

	public function GetBusinessStatusInfo($_array){

		$this->checkCertAndPass();
		$identity=$this->identity->getIdentity();
		$resource=$_array+["Identity"=>$identity];
	
		$response=$this->client->request('POST', 'GetBusinessStatusInfo',['cert'=>[$this->certificate,$this->password],'json' => $resource]);
		
		if( $response->getStatusCode()!==200){
			throw new \Exception("HTTP return Error: ".$response->getStatusCode());
			return null;
		}

		return json_decode($response->getBody()->getContents(),true);

	}


	public function GetAnalyticsByStoreId($storeId){
		if(is_string($storeId)){
			$StoreIds[]=$storeId;
		} else {
			$StoreIds=$storeId;
		}

		$_array=["PageNumber"=>1,
			 	"PageSize"=>100,
				"CriteriaType"=>"SearchByStoreIds",
				"StoreIds"=> $StoreIds,
			 	"TrackingId"=> $this->trackingId
				];

		return $this->GetAnalyticsBase($_array);
	}

	public function GetAnalyticsBatches(){
		$_array=["PageNumber"=>1,
			 	"PageSize"=>100,
				"CriteriaType"=>"GetInBatches",
			 	"TrackingId"=> $this->trackingId
				];
		return $this->GetAnalyticsBase($_array);
	}

	public function GetAnalyticsBase($_array){

		$this->checkCertAndPass();
		$identity=$this->identity->getIdentity();
		$resource=$_array+["Identity"=>$identity];
	
		$response=$this->client->request('POST', 'GetAnalytics',['cert'=>[$this->certificate,$this->password],'json' => $resource]);
		
		if( $response->getStatusCode()!==200){
			throw new \Exception("HTTP return Error: ".$response->getStatusCode());
			return null;
		}

		return json_decode($response->getBody()->getContents(),true);

	}


	public function DeleteBusinesses($storeId){
		if(is_string($storeId)){
			$StoreIds[]=$storeId;
		} else {
			$StoreIds=$storeId;
		}

		$_array=[
				"StoreIds"=> $StoreIds,	
			 	"TrackingId"=> $this->trackingId];

		$this->checkCertAndPass();
		$identity=$this->identity->getIdentity();
		$resource=$_array+["Identity"=>$identity];
	
		$response=$this->client->request('POST', 'DeleteBusinesses',['cert'=>[$this->certificate,$this->password],'json' => $resource]);
		
		if( $response->getStatusCode()!==200){
			throw new \Exception("HTTP return Error: ".$response->getStatusCode());
			return null;
		}

		return json_decode($response->getBody()->getContents(),true);
	}


	public function CreateBulkChain(Chain $chain){
		$this->checkCertAndPass();
		$identity=$this->identity->getIdentity();
		
		$resource=[
			"ChainInfo"=>$chain->getChains(),
			"TrackingId"=>$this->trackingId,
			"Identity"=>$identity
		];
	
		$response=$this->client->request('POST', 'CreateBulkChain',['cert'=>[$this->certificate,$this->password],'json' => $resource]);
		
		if( $response->getStatusCode()!==200){
			throw new \Exception("HTTP return Error: ".$response->getStatusCode());
			return null;
		}

		return json_decode($response->getBody()->getContents(),true);

	}

	public function UpdateBulkChainInfo(Chain $chain){
		$this->checkCertAndPass();
		$identity=$this->identity->getIdentity();
		
		$resource=[
			"ChainInfo"=>$chain->getChains(),
			"TrackingId"=>$this->trackingId,
			"Identity"=>$identity
		];
	
		$response=$this->client->request('POST', 'UpdateBulkChainInfo',['cert'=>[$this->certificate,$this->password],'json' => $resource]);
		
		if( $response->getStatusCode()!==200){
			throw new \Exception("HTTP return Error: ".$response->getStatusCode());
			return null;
		}

		return json_decode($response->getBody()->getContents(),true);

	}

	private function checkCertAndPass(){
		if(empty($this->certificate) || empty($this->password)){
			throw new \Exception("Certificate file and password should be defined for this API");
		}
	}
}

?>