<?php

namespace cat_crash\bingplaces_business;

Class Chain {
	

	public $ChainName;
	public $Website;
	public $Locations;
	public $ClientContactName;
	public $ClientCorporateEmail;

	private $required=[ 'ChainName',
						'Website',
						'Locations',
						'ClientContactName',
						'ClientCorporateEmail'];


	public function __construct($settings=null){
		if(is_array($settings)){
			if(array_key_exists('ChainName', $settings) && array_key_exists('Website', $settings) && array_key_exists('Locations', $settings) && array_key_exists('ClientContactName', $settings) && array_key_exists('ClientCorporateEmail', $settings)){
				
				while(list($key,$value)=each($settings)){
					if(!empty($key)){
						call_user_func([$this,'set'.$key], $value);
					}
				}

			} else {
				throw new \Exception("All Chain attributes have to be defined");
			}
		}

	}


	public function setChainName($value){
		$this->ChainName=$value;
	}

	public function setWebsite($value){
		$this->Website=filter_var($value, FILTER_VALIDATE_URL);
	}

	public function setLocations($value){
		$value=filter_var($value, FILTER_VALIDATE_INT);
		if($value<11){
			throw new \Exception("Minimal locations amount is 10");
			
		}
		$this->Locations=$value;
	}


	public function setClientContactName($value){
		$this->ClientContactName=$value;
	}


	public function setClientCorporateEmail($value){
		$this->ClientCorporateEmail=$value;
	}


	public function getChains(){
		return ['ChainName'=>$this->ChainName,
				'Website'=>$this->Website,
				'Locations'=>$this->Locations,
				'ClientContactName'=>$this->ClientContactName,
				'ClientCorporateEmail'=>$this->ClientCorporateEmail
				];
	}

	public function __destruct(){
		foreach($this->required as $required){
			if(empty($this->$required)){
				throw new \Exception("Property ".$required." is required");		
			}
		}
	}

}